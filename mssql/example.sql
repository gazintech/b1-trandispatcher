/* ---------------------------------------------------------
This is a sample check for the GazIntech transaction-
notification dispatcher.  It assumes that the dispatcher is
invoked with the prefix 'GT_TN_', although you may use any
other prefix.  The check requires that the name of a
business partner not exceed five characters.

In this wise you can extract individual tests from
SBO_SP_TransactionNotification into separate procedures,
which the dispatcher will invoke automatically.
--------------------------------------------------------- */

IF Object_ID(    '"GT_TN_BP_PreventLongNames_.2."') IS NOT NULL
   DROP PROCEDURE "GT_TN_BP_PreventLongNames_.2."
   GO

-- `2' is the B1 type of the Business partner object:
CREATE PROCEDURE "GT_TN_BP_PreventLongNames_.2."
(	@type    NVARCHAR(30), 				
	@key     NVARCHAR(255),
	@action  NVARCHAR(1),
	@errtext NVARCHAR(200)   OUTPUT
)
/* Copyright 2018-20             GazIntech LLC              info@gazintech.ru */
/* Usage of the works is permitted  provided that this instrument is retained */
/* with the works, so that any entity that uses the works is notified of this */
/* instrument.     DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.                */
AS
DECLARE @cardName AS varchar(10)
DECLARE @maxLen   AS int

SET @maxLen = 5
BEGIN
	IF @action NOT IN ('A', 'U') RETURN

	SELECT @cardName = COALESCE( CardName ,' ')
	FROM OCRD WHERE CardCode = @key
	
	IF LEN( @cardName ) >= @maxLen
	
	SET @errtext = N'Business partner name shall not be longer than ' +
	               CAST(@maxLen AS varchar(4)) + ' characters.'
END

