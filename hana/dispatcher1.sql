/* ----------- Dispatcher of transaction notifications for HANA 1 ----------  */

DROP PROCEDURE GT_TranDispatcher;
CREATE PROCEDURE GT_TranDispatcher
(	IN  pref     VARCHAR  (32),
	IN  objType  NVARCHAR (30),
	IN  objKey   NVARCHAR (255),
	IN  action   NVARCHAR (1),
	OUT errCode  INT,
	OUT errText  NVARCHAR (200)
)
/* Copyright 2018-20             GazIntech LLC              info@gazintech.ru */
/* Usage of the works is permitted  provided that this instrument is retained */
/* with the works, so that any entity that uses the works is notified of this */
/* instrument.     DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.                */
/*                                                                            */
/* This project in Gitlab: https://gitlab.com/gazintech/b1-trandispatcher     */
AS	invPrefix    NVARCHAR (256) =
	'	DO BEGIN
		DECLARE error NVARCHAR(200);
	';
	invPostfix   NVARCHAR(256) =
	'	IF COALESCE( :error, '''' ) <> '''' THEN
			SET ''GTTNRES'' = :error;
		END IF;
		END;
	';
	procName     VARCHAR  ( 255);
	procNameS    VARCHAR  ( 255);
	params       NVARCHAR ( 255);
	invocation   NVARCHAR (1024);	
	errFull      NVARCHAR (4096);
	errCodeS     VARCHAR  (  50);
	tokLoc       INT;
	maxLen       INT;
	entryN       INT;
	exHdrPos     INT;
	exBodyPos    INT;
	exPref       NVARCHAR ( 256);
	exText       NVARCHAR (4096);

	CURSOR procs ( c_pref VARCHAR(32), c_schema VARCHAR (64 ) ) FOR
	SELECT NAME FROM SYS.P_PROCEDURES_
	WHERE
		SCHEMA = :c_schema AND 
		NAME LIKE :c_pref || '%.' || :objType || '.%' ESCAPE '\';
		
BEGIN SEQUENTIAL EXECUTION
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		errCode := ::SQL_ERROR_CODE;
		errText := 'Error in GT_TranDispatcher: ' ||
			SUBSTRING(::SQL_ERROR_MESSAGE, 0, 171);
	END;
	
	params := '(' || '''' || :objType || ''', ' || '''' || :objKey ||
	          ''',' || '''' || :action || ''',' || 'error' || ')';
	         
	errCode := 0;
	errText := 'All is well.';
	
	OPEN procs( :pref, CURRENT_SCHEMA );	
	WHILE 1 = 1 DO
		FETCH procs INTO procName;
		IF procs::NotFound THEN BREAK; END IF;
		invocation := 'CALL ' || '"' || :procName || '"' || :params || ';' ;
		invocation := :invPrefix || :invocation || :invPostfix;
		procNameS  :=  TRIM( '_' FROM SUBSTR_BEFORE( :procName, '.' ) );
		BEGIN
			DECLARE EXIT HANDLER FOR SQLEXCEPTION
			BEGIN
				errCode   := ::SQL_ERROR_CODE;
				exText    := ::SQL_ERROR_MESSAGE;
				exPref    := :procName || '":';
				exHdrPos  := LOCATE( :exText, :exPref  );
				exBodyPos := :exHdrPos + LENGTH( :exPref );
				IF :exHdrPos > 0
					THEN errFull := SUBSTRING( :exText, :exBodyPos );
					ELSE errFull := :exText;
				END IF;
			END;
			EXEC :invocation;			
			errFull := SESSION_CONTEXT('GTTNRES');
			IF :errFull IS NOT NULL THEN
				errCode := 1;
				UNSET 'GTTNRES';
			END IF;
		END;
		IF :errCode <> 0 THEN
			maxLen   := 200 - LENGTH( :procNameS ) - 3;
			IF LENGTH( :errFull ) > :maxLen THEN
				errFull := SUBSTRING( :errFull, 1, :maxLen - 3 ) || '...';
			END IF;
			errText := :errFull || ' [' || :procNameS || ']';	
			BREAK;					
		END IF;
	END WHILE;
	CLOSE procs;
END;
