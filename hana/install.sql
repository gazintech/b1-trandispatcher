/* ---------------------------------------------------------
The sample code below shows how to install the GazIntech
transaction-notification dispatcher to a B1 company
database.  Simply invoke GT_TranDispatcher from the desired
call-back procedure, which may be either

  a.  SBO_SP_TransactionNotification or
  b.  SBO_SP_PostTransactionNotice,

with a special prefix.  Our convention, for instance, is to
use `GT_TN_' for the former and `GT_PTN_' for the latter.
--------------------------------------------------------- */

DROP   PROCEDURE SBO_SP_TransactionNotification;
CREATE PROCEDURE SBO_SP_TransactionNotification
(	IN object_type              NVARCHAR( 30),  -- SBO Object Type
	IN transaction_type         NCHAR   (  1),  -- [A]dd, [U]pdate, [D]elete, [C]ancel, C[L]ose
	IN num_of_cols_in_key       INT,
	IN list_of_key_cols_tab_del NVARCHAR(255),
	IN list_of_cols_val_tab_del NVARCHAR(255)
)
AS	error         INT;
	error_message NVARCHAR (200);

BEGIN
	error         := 0;
	error_message := N'Ok';
	
	-- Dispatch to procedures starting with `GT_TN_'
	-- using the backslash as the escape character for LIKE wildcards:
	CALL GT_TranDispatcher( 'GT\_TN\_',
		:object_type, :list_of_cols_val_tab_del, :transaction_type,
		 error,        error_message ); 

	IF :error = 0 THEN
	/* Move existing checks here until you convert them to GT_TN_* procedures */
	END IF;
	
	SELECT :error, :error_message FROM DUMMY;
END;

