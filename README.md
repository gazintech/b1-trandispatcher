# `b1-trandispatcher`

## Introduction

*SAP Business One* (henceforth *B1*) has a facility for the execution of
arbitrary *SQL* code at the completion of a database transaction.
It comprises two stored procedures: `SBO_SP_TransactionNotification`,
invoked prior to ending a transaction, and
`SBO_SP_PostTransactionNotice`, invoked upon a commit of a transaction.
The first procedure can additionally request that the transaction be
rolled back, thus providing a means to implement custom constraints in
business logic. The procedures may be considered as event handlers, for 
*B1* invokes them for every operation upon every business object.

**`b1-trandispatcher`** a dispatcher of transaction notifications for *SAP
Business One* that works on both *MSSQL* and *HANA* and is designed to
relieve the implementer of the burdens attenant upon the maintenance
of a large number of complicated constraints in a single monolithic
procedure, by offering:

1. decentralised specification of constraints as separate stored
   procedures that need no explicit invocation from a main procedure,
   nor any other “registration” in the system,
2. optimal execution of constrants with no redundancy, and
3. accurate handling and reporing of errors inside *B1* client.

At the same time, the dispatcher is very light, consisting of a single
stored procedure that does not depend upon any custom metadata.  It is
free for anyone to use under the terms of the
[Fair license](https://opensource.org/licenses/Fair).

## Motivation 

As the amount of custom logic implemented in the aforesaid procedures
increases, they become larger and more difficult to maintain.  The
need for several developers to work on those cumbersome procedures—often
simultaneolsly—aggravates the situation, inviting errors due to
insufficient coordination and the usual difficulties of concurrent
editing, including outright data loss as a result of conflicting
modifications.

The stark discipline required to keep the procedures well-stuctured and
efficent is in practice very hard, if not impossible, to enforce because
every party on the project will often have need to add their own
constraints. It is not an exception on our projects for several
implementers from different companies to work on
`SBO_SP_TransactionNotification` together with specialists from the
client itself. Good coordination is very difficult in these
circumstances. Without it, however, the transaction-handling procedures
inevitably degrade into imperious walls of unmanageable [spaghetti
code](https://wiki.c2.com/?SpaghettiCode).

Futhermore, the disorderly code in `SBO_SP_TransactionNotification` is
often written without consideration of efficency, so that, after a
constraint has requested a rollback, it continues the now-redundant
execution to the end instead of terminating immediately.

The several solutions to this problem that we have met with are rather
clumsy in that they rely on custom metadata and/or custom applicatons to
manage individual constraints.

Our solution is lean and clean: it does not contamintate the database
with new tables, nor eforces the installation of a special management
application. Instead, it facilitates the divide-and-conquer principle by
storing individual constraints as stored procedures amenable to
management by the native facilities of the RDBMS. 

## Usage
Usage of the dispatcher is very simple. To add a constraint, one need
only implement a stored procedure with a special name and prototype. The
name of the dispatcher procedure shall be of the form:

    <prefix><name><objtypes>

where `<prefix>` is a customisable string that identifies
constraint-procedures for the dispatcher. Use the backslash character
(`\`) to escape `LIKE` wildcards.
We at *GazIntech*, for
instance, use the prefix `GT_TN_` for constraints invoked from
`SBO_SP_TransactionNotification` and `GT_PTN_` for handlers invoked from
`SBO_SP_PostTransactionNotice`. `<name>` is a human-oriented procedure
name that should describe the essense of the constraint, and
`<objtypes>` is a list of *B1* object types to which the constrant applies.
The types shall be delimited, and their list surrouned, by full stops
(see *Example*).

In *HANA*, the procedure shall have the following prototype:

    (   IN  objType  NVARCHAR ( 30), -- B1 object type
        IN  objKey   NVARCHAR (255), -- key of B1 object
        IN  action   NVARCHAR (  1), -- action ('A','U','C','L', or 'D')
        OUT error    NVARCHAR (200)  -- error message
    )

The prototype of an *MSSQL* procedure is identical—refer to
`mssql/example.sql` for a working example.

The procedure shall signal a rollback of the transaction by filling the
`error` output parameter with a non-empty error message. The dispatcher
will block the transaction and cause this message to be displayed in the
*B1* status bar.  The name of the blocking procedure, with the list of
object types removed for brevity, it will append to the error message in
order that the administrator may know which constraint blocked the
operation.

If an exception should occur in one of such procedures, the dispatcher
will block the operation and show the exception details in the error
message, also appending the short name of the procedure that failed.

## Example
For example, a business constraint that prevents empty *Item* names may be
implemented as the following stored procedure in *HANA*:

    CREATE PROCEDURE "GT_TN_NoEmptyItemNames.4."
    (   IN  objType  NVARCHAR ( 30),
        IN  objKey   NVARCHAR (255),
        IN  action   NVARCHAR (  1),
        OUT error    NVARCHAR (200)
    )
    AS itemname NVARCHAR(100);
    BEGIN
        -- We are interested in the [A]dd and [U]pdate actions:
        IF :action <> 'A' AND :action <> 'U' THEN RETURN; END IF;
        
        -- Read the item name into `itemname':
        SELECT "ItemName" INTO itemname FROM OITM
        WHERE "ItemCode" = :objKey;
        
        -- If the name is NULL or empty, indicate an error
        -- by setting the `error' variable:
        IF COALESCE( :itemname, '' ) = '' THEN
            error := 'Item name cannot be empty.';
        END IF;
    END;

Once the procedure is created, the dispatcher will *automatically*
invoke it whenever an *Items* object is affected in *B1*—no futher
registration of the constraint is required. If one now tries to add
an *Item* with an empty name *B1* will block the operation and show the
following error message in the status bar:

    (1) Item name cannot be empty. [GT_TN_NoEmptyItemNames]

To test error handling, one may change the query so that it always fails:

    SELECT "ItemName" INTO itemname FROM OITM
    WHERE "ItemCode" = NULL;

and try again to create an *Item* with an empty name. The dispatcher
will again block the operation, but this time the error message will
contain details about the exeption:

    (1299) line 13 col 9 (at pos 437): [1299] (range 3)
    no data found exception: no data found [GT_TN_NoEmptyItemNames]

See `mssql/example.sql` for an equivalent procedure in `T-SQL`.

## Installation
To install the dispacher,

1. apply the dispatcher script to the desired database:
   it is named `dispatcher.sql` for *MSSQL*, `dispatcher1.sql` for
   *HANA 1*, and `dispatcher2.sql` for *HANA 2*.
2. add an invocation of the dispathcer to the desired *B1* procedure as
   shown in `install.sql`.

## A note on performace
Whereas procedure invocations are always slower than direct *SQL*, and
dynamic *SQL* than static *SQL*, this dispatcher of transaction
notificaions is naturally less efficient than a manually maintained
monolithic procedure. If, however, the total number of constraints is
within a thousand, and no more than fifteen are executed for each
specific object type, the performace loss due to the dispatcher is
negligible and often unnoticeable. This capacity has been more than
sufficient on all our projects.

Of course, the *SQL* code of the individual procedures should be well
optimised.

## Known problems

### Handling of exceptions in MSSQL

Although the *MSSQL* version of the dispatcher handles exceptions in
exactly the same way as the *HANA* version, *B1* for *MSSQL* will not,
for some reason, display them in the status bar, showing a meaningless
“internall error” instead. We have complained about it to SAP support,
but they replied that it is the intended behavior and closed the
incident.

Nevertheless, one can always read exception details by invoking
`SBO_SP_TransactionNotification` manually.

### Access to `SYS.P_PROCEDURES_` from *HANA 2*

To speed up execution, the dispatcher for *HANA 1* accesses
stored procedures directly via the table `SYS.P_PROCEDURES_`. Although
it works in *HANA 2* as well when invoking the dispatcher
manually from *HANA Studio*, it does not work from *B1*. This made us
publish a separate version of the dispacher for *HANA 2*.
It uses the `PROCEDURES` view, which is—naturally—slower.

## Contact

Feel free to report errors in and suggest improvements to 
`b1-trandispatcher` by creating a *Gitlab* issue for the project.

## Manual maintenance of `SBO_SP_TransactionNotification`

We offer the following recommendations for whoever cannot, or does not
wish to, use the dispatcher. They have all the drawbacks mentioned in
*Motivation*, but are desinged to satisfy at least the following
requirements:

1. readable, regular, and easily maintainable code structure, and
2. efficiency, so that the execution terminates at the first constraint
   that blocks the transaction.

### 1. A monolithic procedure

For *MSSQL*, one solution is to terminate execution by means of `GOTO`
statmenets, i.e.:

    SET @error = 1 -- set error flag
    
    IF @object_type = '17' AND @transaction_type = 'A'
    BEGIN
        /* test 1 declarations */
        /* test 1 code         */
        IF /* test 1 condition */ 
        BEGIN
            SET @error_message = 'Test 1 failed...'
            GOTO Block
        END
    END

    /* ... more tests ... */

    IF @object_type = '17' AND @transaction_type IN ('A', 'U')
    BEGIN
        /* test N declarations */
        /* test N code         */
        IF /* test N condition */ 
        BEGIN
            SET @error_message = 'Test N failed...'
            GOTO Block
        END
    END

    SET @error = 0 -- clear error flag, if no GOTO executed
    Block: SELECT @error, @error_message

Even though the scope of *T-SQL* variables
[extends](https://docs.microsoft.com/en-us/sql/t-sql/language-elements/variables-transact-sql)
until the end of the proecedure, we recommend separate declarations for
each constraint—for better readability and easier maintenance.  Just
make certain to adopt a consistent naming scheme to avoid name
collisions. For example, you may encode the object type and transaction
type in a prefix: `@A17_cardcode` for *test 1* and `@AU17_CardCode` for
*test 2* above. In the long run, it shall prove much better than resolving
name collisions through invention of arbitrary names and debugging errors
due to accidental reuse of a variable with a too-long lifespan.

Although *HANA* (at least *HANA 1.0*) does not suport the `GOTO`
statement, the same behavior can be implemented in it by means of
multiple `BREAK` statements inside a `WHILE` loop:

    error := 1; -- set error flag
    WHILE 1 = 1 DO
        IF :object_type = '17' AND :transaction_type = 'A' THEN
            /* test 1 declarations */
            /* test 1 code         */
            IF /* test 1 condition */ THEN
                error_message := 'Test 1 failed...';
                BREAK;
            END IF;
        END IF;

        /* ... more tests ... */

        IF :object_type = '17' AND LOCATE( 'AU', :transaction_type ) > 0 THEN
            /* test N declarations */
            /* test N code         */
            IF /* test N condition */ THEN
                error_message := 'Test N failed...';
                BREAK;
            END IF;
        END IF;

        error := 0; -- clear error flag, if no BREAK executed
        BREAK;
    END WHILE;

    SELECT :error, :error_message FROM DUMMY;

One advantage of *SQLScript* over *T-SQL* is proper scoping of
variables, so that the programmer need not fear name collisions between
variables declared in different blocks.

### 2. One procedure per constrant

Another approach is to emulate by hand what the dispatcher does
automatically—e.g. in *HANA*:

    error_message := '';
    WHILE 2 * 2 = 4 DO

        IF :object_type = '17' AND :transaction_type = 'A' THEN
            CALL Constraint1( ..., error_message );
            IF :error_message <> '' THEN BREAK; END IF;
        END IF;

        /* ... more tests ... */

        IF :object_type = '17' AND LOCATE( 'AU', :transaction_type ) > 0 THEN
            CALL ConstraintN( ..., error_message );
            IF :error_message <> '' THEN BREAK; END IF;
        END IF;
        BREAK;

    END WHILE;

    SELECT :error, :error_message FROM DUMMY;

where each `Constraint` procedure takes the error message as an `OUTPUT`
parameter.
