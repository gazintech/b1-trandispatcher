IF Object_ID(    'GT_TranDispatcher') IS NOT NULL
   DROP PROCEDURE GT_TranDispatcher
   GO

CREATE PROCEDURE GT_TranDispatcher
(	@prefix  VARCHAR(  32),
	@type    NVARCHAR( 30), 				
	@key     NVARCHAR(255),
	@action  NVARCHAR(  1),
	@errCode INT            OUTPUT,
	@errText NVARCHAR(200)  OUTPUT
)
/* Copyright 2018-20             GazIntech LLC              info@gazintech.ru */
/* Usage of the works is permitted  provided that this instrument is retained */
/* with the works, so that any entity that uses the works is notified of this */
/* instrument.     DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.                */
/*                                                                            */
/* This project in Gitlab: https://gitlab.com/gazintech/b1-trandispatcher     */
AS
DECLARE @procName      AS VARCHAR (255)
DECLARE @procNameS     AS VARCHAR (255)
DECLARE @paramTemplate AS VARCHAR (255)
DECLARE @params        AS NVARCHAR(255)
DECLARE @sourceProc    AS VARCHAR (255)
DECLARE @outParDef     AS NVARCHAR(255)
DECLARE @inverrtxt     AS NVARCHAR(255)
DECLARE @maxLen        AS INT

SET @sourceProc = ''
SET @params     = '@type, @key, @action, @inverrtxt OUTPUT'
SET @outParDef  =
'	@type      NVARCHAR(30), 				
	@key       NVARCHAR(255),
	@action    NVARCHAR(1),
	@inverrtxt NVARCHAR(200)  OUTPUT
'
DECLARE @invocation AS NVARCHAR(255)
DECLARE Procs CURSOR STATIC FOR
	SELECT name FROM sys.procedures
	WHERE
		[type] = 'P' AND
		name LIKE  @prefix + '%.' + @type + '.%' ESCAPE '\'
	
SET @errCode   = 0;
SET @errText   = 'All is well.';
SET @inverrtxt = '';
OPEN Procs

WHILE ( 1 = 1 )
BEGIN
	FETCH NEXT FROM Procs INTO @procName
	IF (@@FETCH_STATUS <> 0) BREAK
	SET @invocation = 'EXEC "' + @procName + '" ' + @params
	SET @procNameS  =  SUBSTRING(@procName, 1, CHARINDEX('.', @procName) - 2)
	
	BEGIN TRY
		EXEC sp_executesql @invocation, @outParDef,
			@type = @type, @key = @key, @action = @action,
			@inverrtxt = @inverrtxt OUTPUT
	
	END TRY
	BEGIN CATCH
		IF @procName <> ERROR_PROCEDURE()
		SET @sourceProc = '->' + ERROR_PROCEDURE()
		SET @errCode = ERROR_NUMBER();
		SET @errText = @procNameS + @sourceProc + ': line '
			+ CAST(ERROR_LINE() AS VARCHAR(8)) + ': ' + ERROR_MESSAGE()
				
		BREAK
	END CATCH
	
	IF @errCode = 0 AND @inverrtxt <> '' -- Check failed
	BEGIN
		SET @errCode = 1
		SET @errText = @inverrtxt
	END
	
	IF @errCode <> 0
	BEGIN
		SET @maxLen = 200 - LEN( @procNameS ) - 3;
		IF LEN( @errText ) > @maxLen
			SET @errText = SUBSTRING(@errText, 0, @maxLen - 3 ) + '...'
		SET @errText = @errText + ' [' +  @procNameS + ']'
		
		BREAK
	END
END

CLOSE Procs
DEALLOCATE Procs

