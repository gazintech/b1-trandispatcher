/* -----------------------------------------------------------------------------
This is a sample check for the GazIntech transaction- notification dispatcher.
It assumes that the dispatcher is invoked with the prefix 'GT_TN_', although
you may use any other prefix.  The check requires that the name of an Item not
be empty.

In this wise you can extract individual tests from
SBO_SP_TransactionNotification into separate procedures, which the dispatcher
will invoke automatically.
----------------------------------------------------------------------------- */

-- `4' is the type of Item Master Data (OITM.ObjType):
DROP   PROCEDURE "GT_TN_NoEmptyItemNames.4.";
CREATE PROCEDURE "GT_TN_NoEmptyItemNames.4."
(	IN  objType  NVARCHAR ( 30), -- B1 object type
	IN  objKey   NVARCHAR (255), -- object key
	IN  action   NVARCHAR (  1), -- action ('A', 'U', 'C', or 'L')
	OUT error    NVARCHAR (200)  -- error message
)
/* Copyright 2018-20             GazIntech LLC              info@gazintech.ru */
/* Usage of the works is permitted  provided that this instrument is retained */
/* with the works, so that any entity that uses the works is notified of this */
/* instrument.     DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.                */

AS itemname NVARCHAR(100);
BEGIN
	-- We are interested in the [A]dd and [U]pdate actions:
	IF :action <> 'A' AND :action <> 'U' THEN RETURN; END IF;
	
	-- Read the item name into `itemname':
	SELECT "ItemName" INTO itemname FROM OITM
	WHERE "ItemCode" = :objKey;
	
	-- If the name is NULL or empty, indicate an error
	-- by setting the `error' variable:
	IF COALESCE( :itemname, '' ) = '' THEN
		error := 'Item name cannot be empty.';
	END IF;
END;

