/* ---------------------------------------------------------
The sample code below shows how to install the GazIntech
transaction-notification dispatcher to a B1 company
database.  Simply invoke GT_TranDispatcher from the desired
call-back procedure, which may be either

  a.  SBO_SP_TransactionNotification or
  b.  SBO_SP_PostTransactionNotice,

with a special prefix.  Our convention, for instance, is to
use `GT_TN_' for the former and `GT_PTN_' for the latter.
--------------------------------------------------------- */

IF Object_ID(    'SBO_SP_TransactionNotification') IS NOT NULL
   DROP PROCEDURE SBO_SP_TransactionNotification;
   GO

CREATE PROCEDURE SBO_SP_TransactionNotification
(	@object_type              NVARCHAR( 30), -- SBO Object Type
	@transaction_type         NCHAR   (  1), -- [A]dd, [U]pdate, [D]elete, [C]ancel, C[L]ose
	@num_of_cols_in_key       INT,
	@list_of_key_cols_tab_del NVARCHAR(255),
	@list_of_cols_val_tab_del NVARCHAR(255)
)
AS
BEGIN
	DECLARE @error         INT -- 0 for no error
	DECLARE @error_message NVARCHAR (200)

	-- Dispatch to procedures starting with `GT_TN_'
	-- using the backslash as the escape character for LIKE wildcards:
	EXEC GT_TranDispatcher 'GT\_TN\_',
		@object_type,      @list_of_cols_val_tab_del, @transaction_type,
		@error OUTPUT,     @error_message OUTPUT

	IF @error = 0
	BEGIN
		-- Move existing checks here until you convert them to GT_TN_* procedures
		SET @error = 0 -- MS SQL Server balks at empty BEGIN..END blocks...
	END

	SELECT @error, @error_message
END

